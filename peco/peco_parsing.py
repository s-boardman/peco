import pandas as pd
import numpy as np


PECO_DTYPES = {
    'round': 'int32',
    'bib':  'int32',
    'pos': 'int32',
    'name': 'object',
    'category_position': 'object',
    'category': 'category',
    'club': 'category',
    'contest': 'category',
    'guest': 'category',
    'second_claim': 'category',
}


def import_peco_json(filepath):
    """Read json of peco results and return a pandas dataframe."""
    # Read the json and then convert the column data types
    peco_df = (
        pd.read_json(filepath, orient='records').astype(dtype=PECO_DTYPES)
    )
    # Do the time delta separately for some reason
    peco_df['time'] = pd.to_timedelta(peco_df['time'])
    # Split the category position into separate columns and convert to ints
    peco_df[['category_pos', 'category_total']] = (
        peco_df.category_position.str.split('/', expand=True)
    )
    peco_df['category_pos'] = peco_df['category_pos'].astype('int32')
    peco_df['category_total'] = peco_df['category_total'].astype('int32')
    # Rearrange columns for neatness
    peco_df = peco_df[
        ['round', 'pos', 'bib', 'name', 'club', 'second_claim', 'guest',
         'contest', 'category', 'category_pos', 'category_total', 'time']
    ]
    # Convert finish time to seconds
    peco_df['finish_time_secs'] = peco_df['time'].astype(int) / 1E9
    return peco_df


def calculate_counter_pos(peco_df):
    """The counter pos excludes guest runners."""
    # Need to create a df without guest runners, recalculate the positions
    # Merge back with original df, ensure that guest runners have a
    # counter_pos of None
    updated_dfs = []
    for g, df in peco_df.groupby(by=['round', 'contest']):
        n = 1.0
        counter_pos_list = []
        for (i, runner) in df.iterrows():
            if runner.guest == 'Yes':
                counter_pos_list.append(np.nan)
            else:
                counter_pos_list.append(n)
                n += 1
        df = df.join(
            pd.Series(
                data=counter_pos_list, index=df.index, name='counter_pos',
                dtype='float32'
            ).to_frame()
        )
        updated_dfs.append(df)
    peco_counter_scoring_df = pd.concat(updated_dfs).sort_index()
    return peco_counter_scoring_df


def count_club_relative_placing(peco_df):
    """Calculate the club finishing position for each runner."""
    # Create the club counts for scoring
    updated_dfs = []
    for g, df in peco_df.groupby(by=['round', 'contest', 'club']):
        club_numbers_list = range(1, len(df.index) + 1)
        df = df.join(
            pd.Series(
                data=club_numbers_list, index=df.index, name='club_pos',
                dtype='int32'
            ).to_frame()
        )
        updated_dfs.append(df)
    peco_club_scoring_df = pd.concat(updated_dfs).sort_index()
    return peco_club_scoring_df


DIVISION_DTYPES = {
    'club': 'category',
    'division': 'category',
    'contest': 'category',
}


def import_peco_division_csv(filepath):
    """Read the division csv file into a dataframe."""
    division_df = pd.read_csv(filepath, header=0)
    return division_df.astype(dtype=DIVISION_DTYPES)


def link_results_with_divisions(peco_df):
    """Merge the peco_df with the division info."""
    division_csv = 'peco/leagues_2018_2019.csv'
    division_df = import_peco_division_csv(division_csv)
    peco_division_df = pd.merge(peco_df, division_df, how='outer')
    # Add None category to division field
    peco_division_df.division.cat.add_categories("None", inplace=True)
    # Fill empty cells with "None" strings
    peco_division_df.division.fillna("None", inplace=True)
    return peco_division_df


CONTEST_LEAGUE_COUNTER_DICT = {
    'Ladies': {
        'Premier': 8,
        '1': 7,
        '2': 6,
    },  
    'Men': {
        'Premier': 9,
        '1': 8,
        '2': 7,
    }
}


def calculate_other_team_counters_beaten(runner, peco_df):
    """Count number of counting runners the given runner is ahead of."""
    # Determine counter limit
    try:
        counter_limit = (
            CONTEST_LEAGUE_COUNTER_DICT[runner['contest']][runner['division']]
        )
    except KeyError:
        counter_limit = 0
    # Calculate counters from other teams behind
    counters_beaten = len(
        peco_df[
            (peco_df['round'] == runner['round'])
            & (peco_df['contest'] == runner['contest'])
            & (peco_df['division'] == runner['division'])
            & (peco_df['pos'] > runner['pos'])
            & (peco_df['club'] != runner['club'])
            & (peco_df['club_pos'] <= counter_limit)
        ].index
    )
    return counters_beaten


CONTEST_LEAGUE_PENALTY_DICT = {
    'Ladies': 25,  
    'Men': 50
}


def calculate_team_score(club, round, peco_df):
    """Add up the finish position for each team.

    Sum the finishing positions for the team counter score
    If number of counters less than the value in CONTEST_LEAGUE_COUNTER_DICT
    Add in count for none guest runners (for each missing counter)

    """
    # Get the position of the last runner +1 for the penalty counter value
    penalty_counter = len(
        peco_df[
            (peco_df['guest'] != 'Yes')
            & (peco_df['contest'] == club['contest'])
            & (peco_df['round'] == round)
        ].index
    ) + 1
    # Use CONTEST_LEAGUE_COUNTER_DICT value to get counter limit
    try:
        counter_limit = (
            CONTEST_LEAGUE_COUNTER_DICT[club['contest']][club['division']]
        )
    except KeyError:
        counter_limit = 0
    # Get all the runners for a club for the given round (in a contest)
    club_df = peco_df[
        (peco_df['club'] == club['club'])
        & (peco_df['contest'] == club['contest'])
        & (peco_df['round'] == round)
    ].sort_values('club_pos')
    # Iterate through the rows of the club_df and extract the counter_pos
    counter_positions = []
    # A maximum of 1 second claim runner can be included in the counters
    second_claim_runners = 0
    for (i, runner) in club_df.iterrows():
        if runner['guest'] == 'Yes':
            pass
        elif len(counter_positions) < counter_limit:
            if runner['second_claim'] == '2nd':
                second_claim_runners += 1
                if second_claim_runners > 1:
                    pass
                else:
                    counter_positions.append(runner['counter_pos'])
            else:
                counter_positions.append(runner['counter_pos'])
        else:
            pass
    # Get club division
    division = club['division']
    # Calculate if there are any counters missing
    missing_counters = counter_limit - len(counter_positions)
    # Add the penalty counter score and the penalty points per missing counter
    penalty_score = (
        CONTEST_LEAGUE_PENALTY_DICT[club['contest']] * missing_counters
    )
    missing_counters_score = penalty_counter * missing_counters
    # Add the actual counters score to the missing counter score and the
    # penalty score
    total_score = (
        missing_counters_score + penalty_score + sum(counter_positions)
    )
    return total_score


def create_league_table(overall_league_df):
    """Calculate the club finishing position for each round."""
    updated_dfs = []
    for g, df in overall_league_df.groupby(by=['contest', 'division']):
        division_finishing_pos_list = range(1, len(df.index) + 1)
        for round in range(1, 7):
            try:
                df = df.sort_values('round_{}_score'.format(round))
                position_series = pd.Series(
                    name='round_{}_position'.format(round),
                    data=division_finishing_pos_list, index=df.index,
                    dtype='int32'
                )
                df = df.join(position_series.to_frame())
            except KeyError:
                pass
        updated_dfs.append(df)
    league_table_df = pd.concat(updated_dfs).sort_index()
    return league_table_df