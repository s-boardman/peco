# -*- coding: utf-8 -*-
import scrapy
import re

RACE_NUMBER_REGEX = 'Race\s(?P<race_number>\d)\s'

class RacebestSpider(scrapy.Spider):
	name = 'racebest'
	allowed_domains = ['racebest.com']
	start_urls = [
		'http://racebest.com/results/9uwst', # race 1
		'http://racebest.com/results/6g4v3', # race 2
		'https://racebest.com/results/q66zt', # race 3
		'https://racebest.com/results/eyhu9', # race 4
		'https://racebest.com/results/vfcrc', # race 5
	]

	def parse(self, response):
		title = response.xpath('//title/text()').extract_first()
		try:
			round = int(re.search(RACE_NUMBER_REGEX, title).group('race_number'))
		except TypeError:
			round = None
		for row in response.xpath('//*[@class="table table-bordered table-striped results"]//tbody//tr'):
			yield {
				'round': round,
				'pos': row.xpath('td[1]//text()').extract_first(),
				'bib': row.xpath('td[2]//text()').extract_first(),
				'name': row.xpath('td[3]//text()').extract_first(),
				'club': row.xpath('td[4]//text()').extract_first(),
				'contest': row.xpath('td[5]//text()').extract_first(),
				'second_claim': row.xpath('td[6]//text()').extract_first(),
				'guest': row.xpath('td[7]//text()').extract_first(),
				'category': row.xpath('td[8]//text()').extract_first(),
				'category_position': row.xpath('td[9]//text()').extract_first(),
				'time': row.xpath('td[10]//text()').extract_first()
			}
