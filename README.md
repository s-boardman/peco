# PECO Analytics

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/s-boardman%2Fpeco/round_5)

## Introduction

The [PECO XC](http://www.pecoxc.co.uk/) (Cross Country) is a winter running
league based in and around Leeds. It is run over five mass start races and a
relay race later in the spring. The league is split into three divisions for
each gender. Runners compete for their clubs and scores are awarded to
individual runners based on finishing position (the faster you finish the
lower your score) and to teams by summing the individual scores of the first
X runners (called "counters") for each club. The exact number of counters
depends on the division and gender. After each race clubs are ranked based on
their score and given points depending on their finishing order (again, lower
is better). The accumulation of these scores across the series determines the
league table and which clubs are promoted or relegated.

I enjoy running in these races and being part of a team is very different from
"normal" racing, but I wanted to work out a good way to track my own
performance and see if I could find any patterns as to what makes a for a
competitive team.

To do so I have created this repository which consists of the
[Scrapy](https://scrapy.org/) spiders to extract the results, a library of
utility functions to carry out repetitive parsing and manipulation tasks, and
a series of [Jupyter notebooks](https://jupyter-notebook.readthedocs.io/en/stable/)
to visualise the analyses.

## Instructions

To replicate the analyses first clone the repository.

```bash
git clone git@gitlab.com:s-boardman/peco.git

cd peco
```

Use [pipenv](https://pipenv.readthedocs.io/en/latest/) to install the
requirements in a virtual environment.

```bash
pipenv install
```

To get the results of PECO XC 2018/19 run the following commands.

```bash
cd peco

pipenv run scrapy crawl --nolog -o peco_all.json -t json racebest
```

To start the notebook server and run the analyses run the following commands.

```bash
cd ..

pipenv run jupyter lab --no-browser
```

From here follow the instructions in each of the Jupyter notebooks.
